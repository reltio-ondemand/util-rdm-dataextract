
# RDM DATA EXTRACT

## Description

Reference data is a reasonably stable subset of master data, which is relatively non-volatile in nature. It does not have the high volume, velocity or variety of master data.
The RDM Data Extract utility that is used to extract RDM lookup data from a RDM tenant in either CSV or JSON format.


##Change Log

```
#!plaintext

Last Update Date: 22/12/2021
Version: 2.4.2
Description: Updated the Log4j dependency to 2.17.0. Log4j v2.17.0 has fixes for the following vulnerabilities CVE-2021-44228, CVE-2021-45046, CVE-2021-45105

Last Update Date: 27/06/2019
Version: 2.4.1
Description:
1) Standarized the Jar name
2) Standardized the Version
3) Added Client Credential authentication capability

Last Update Date: 27/06/2019
Version: 2.4
Description:
1) Standarized the Jar name
2) Changed the project Group Name
3) Added Proxy capability

Last Update Date: 29/03/2019
Version: 2.3
Description:
1)Implemented a way to encrypt the password stored in properties file.
2)Clear validation message when properties are missing
3)Error message when the user does not pass the property file from the args.


Last Update Date: 17/12/2018
Version: 2.2
Description: Upgraded the Reltio CST Core Version, Standarization of Logs to Log4j2,Remove unused dependency


Last Update Date: 03/08/2018
Version: 2.1.0
Description: Upgraded the Reltio CST Core Version. Changes in properties file as part of Properties Standardization. Renamed RDM_SERVER_HOST to ENVIRONMENT_URL, 
```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
#!plaintext
Copyright (c) 2018 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util-rdm-dataextract/src/a9be1e9f0227c35641353d9e8e7c9085275e4f71/QuickStart.md?at=master&fileviewer=file-view-default).


