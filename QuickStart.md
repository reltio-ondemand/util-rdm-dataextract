# Quick Start 

##Building

The path to build the RDM DATA EXTRACT Utility is 
util-rdm-dataextract/src/main/java/com/reltio/rdm/extract/service/RDMExtract.java

##Dependencies 

1. reltio-cst-core-1.5.0


##Properties File Example - rdm_extract.properties 

```
#!paintext
#Common Properties
ENVIRONMENT_URL=api-dot-prod-155422.appspot.com
TENANT_ID=*******
USERNAME=********
PASSWORD=*******
AUTH_URL=https://auth.reltio.com/oauth/token


#This property used for to get the access token using client_credentials grant type. The value for this property can be obtained by encoding the client name and secret separated by colon in Base64 format. (clientname:clientsecret)
CLIENT_CREDENTIALS=*******


#Tool specific properties
#It will lookup type names in comma separated. Eg. Country, State
LOOKUP_TYPES=
OUTPUT_LOCATION=/Office/Development/Projects/WMG/RDM/Extract/
EXTRACT_TYPE=RAW
HTTP_PROXY_HOST=<<Proxy Host>>
HTTP_PROXY_PORT=<<Proxy Port>>
```

##Executing

Command to start the utility.
Please note that use the latest JAR version
```
#!plaintext

java -jar reltio-util-rdm-dataextract-${version}-jar-with-dependencies.jar rdm_extract.properties > $logfilepath$ 



```
