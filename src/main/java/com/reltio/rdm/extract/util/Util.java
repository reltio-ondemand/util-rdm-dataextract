package com.reltio.rdm.extract.util;

/**
 * Created by Sanjay on 14/09/17.
 */
public class Util {

    // Function to ignore null values

    public static boolean checkNull(String value) {
        if (value != null && !value.trim().equals("")
                && !value.trim().equals("UNKNOWN")
                && !value.trim().equals("<blank>")
                && !value.trim().equals("<UNAVAIL>")
                && !value.trim().equals("#")
                && !value.toLowerCase().trim().equals("null")
                && !value.toLowerCase().trim().equals("\"")) {
            return true;
        }
        return false;
    }
}
