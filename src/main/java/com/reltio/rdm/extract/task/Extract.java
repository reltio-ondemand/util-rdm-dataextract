package com.reltio.rdm.extract.task;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rdm.extract.constants.RDMDataExtractConstants;
import com.reltio.rdm.extract.domain.Attribute;
import com.reltio.rdm.extract.domain.RDMDataExtractConfigProperties;
import com.reltio.rdm.extract.domain.RDMRecord;

public class Extract implements Runnable{
    private static final Logger logger = LoggerFactory.getLogger(Extract.class.getName());

	private static final String baseHeaders = "CODE|DISPLAY_NAME|PARENTS|ATTRIBUTES";
	private static final String OFFSET_LIMIT = "?offset=%s&limit=%s";
	private static final String SEPARATOR = "|";
	private static final String COMMA = ",";
	private static final String EQUAL = "=";
	private static final String SPACE = " ";
	
	private RDMDataExtractConfigProperties properties;
	private ReltioAPIService apiService;
	private String lookupType;
	private String label;
	private static ExtractType extractType;

	public enum ExtractType{
		RAW,
		CSV
	}
	
	public Extract(String lookupType, String label, RDMDataExtractConfigProperties configProperties, ReltioAPIService reltioAPIService, ExtractType type)

	{
		this.properties = configProperties;
		this.apiService = reltioAPIService;
		this.lookupType = lookupType;
		this.label = label;
		extractType = type;
	}

	public void run() {
		logger.debug("Running extract for "+lookupType);
		int offset = 0;
		int limit = RDMDataExtractConstants.RECORDS_PER_POST;
		StringBuilder url = new StringBuilder();
		url.append(properties.getBaseDataloadURL());
		url.append(File.separator);
		url.append(lookupType);
		
		FileOutputStream fos = null;
		BufferedWriter extract = null;
        try {
        	File fout = null;
        	
        	if(extractType == ExtractType.CSV) {
    			fout = new File(properties.getOutputLocation() + File.separator+ lookupType+".csv");
        	}else {
    			fout = new File(properties.getOutputLocation() + File.separator+ lookupType+".json");
        	}
        	
			if(!fout.exists()) {
				fout.createNewFile();
			}
			
			fos = new FileOutputStream(fout);
			extract = new BufferedWriter(new OutputStreamWriter(fos, "UTF8"));
        	if(extractType == ExtractType.CSV) {
    			extract.write(label);
    			extract.newLine();
    			extract.write(baseHeaders);
    			extract.newLine();
        	}

			
			boolean eof = false;
			
			while(!eof) {
				
	        	if(extractType == ExtractType.CSV) {
					List<RDMRecord> responseList = getRDMRecords(url.toString()+String.format(OFFSET_LIMIT, offset, limit));
					offset = offset+limit;
					generateReport(responseList, extract);
					
					if(responseList.size() < limit) {
						eof = true;
					}
	        	}else {
	        		eof = printJson(url.toString()+String.format(OFFSET_LIMIT, offset, limit), extract);
					offset = offset+limit;
	        	}

			}
            
		} catch (Exception e) {
			logger.error("Error in getting RDM data. Error = "+e.getMessage(), e);
		}finally {
			try {
				extract.close();
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		logger.debug("Extract completed for "+lookupType);

	}
	
	private boolean printJson(String url, BufferedWriter extract) throws JsonParseException, JsonMappingException, IOException {
		boolean eof = false;
		for(int i = 0; i < RDMDataExtractConstants.RETRY_COUNT; i++) {
			String response;
			try {
				logger.info("try count for "+lookupType+" = "+i);
				response = apiService.get(url.toString(), null);
		        Gson gson = new Gson();

		        List<JsonObject> responseList = gson.fromJson(response, new TypeToken<List<JsonObject>>() {
		        }.getType());
		        
				if(responseList == null || responseList.size() < RDMDataExtractConstants.RECORDS_PER_POST) {
					eof = true;
				}

				if(responseList != null) {
			        for(JsonObject obj : responseList) {
	                	extract.write("["+obj.toString()+"]");
	                	extract.newLine();
			        }
				}
		        
	            
	            break;
	            
			} catch (GenericException | ReltioAPICallFailureException e) {
				logger.error("Error in getting RDM data. Error = "+e.getMessage(), e);
				continue;
			}
			
		}
		return eof;
	}
	
	private List<RDMRecord> getRDMRecords(String url) {
		List<RDMRecord> responseList = new ArrayList<>();
		
		for(int i = 0; i < RDMDataExtractConstants.RETRY_COUNT; i++) {
			String response;
			try {
				logger.debug("try count for "+lookupType+" = "+i);
				response = apiService.get(url.toString(), null);
		        Gson gson = new Gson();
		        responseList = gson.fromJson(response, new TypeToken<List<RDMRecord>>() {
		        }.getType());
			} catch (GenericException | ReltioAPICallFailureException e) {
				logger.debug("Error in getting RDM data. Error = "+e.getMessage());
				continue;
			}
	        
	        return responseList;
	        
		}
		
        return responseList;
	}

	private void generateReport(List<RDMRecord> responseList, BufferedWriter extract) throws IOException {
		for(RDMRecord record : responseList) {
			extract.append(record.getCode());
			extract.append(SEPARATOR);
			extract.append(record.getSourceMappings().get(0).getValues().get(0).getValue());
			
			extract.append(SEPARATOR);
			if(record.getParents() != null && record.getParents().size() > 0) {
				for(String parent : record.getParents()) {
					extract.append(parent.split("/")[2]+File.separator+parent.split("/")[3]);
					extract.append(COMMA);
				}
			}
			
			extract.append(SEPARATOR);
			if(record.getAttributes() != null && record.getAttributes().size() > 0) {
				for(Attribute attr : record.getAttributes()) {
					extract.append(attr.getName()+SPACE+EQUAL+SPACE+attr.getValue());
					extract.append(COMMA);
				}
			}
			
			extract.newLine();
		}
	}

	public static ExtractType getExtractType() {
		return extractType;
	}

	public static void setExtractType(ExtractType extractType) {
		Extract.extractType = extractType;
	}
}
