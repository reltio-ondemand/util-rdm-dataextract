package com.reltio.rdm.extract.constants;

import com.google.gson.Gson;

/**
 * Created by Sanjay on 14/09/17.
 */
public final class RDMDataExtractConstants {

    public static final Integer RETRY_COUNT = 5;
    public static final Integer RECORDS_PER_POST = 100;
    public static final Integer THREAD_COUNT = 15;
    public static final Gson GSON = new Gson();
	public static final Integer MAX_QUEUE_SIZE_MULTIPLICATOR = 10;
}
