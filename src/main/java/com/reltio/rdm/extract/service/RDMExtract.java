package com.reltio.rdm.extract.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;
import com.reltio.rdm.extract.constants.RDMDataExtractConstants;
import com.reltio.rdm.extract.domain.LookupResult;
import com.reltio.rdm.extract.domain.LookupType;
import com.reltio.rdm.extract.domain.RDMDataExtractConfigProperties;
import com.reltio.rdm.extract.task.Extract;


/**
 * 
 * Created by Sanjay on 14/09/17.
 */
public class RDMExtract {

    private static final Logger LOGGER = LoggerFactory.getLogger(RDMExtract.class.getName());

    public static void main(String[] args) throws Exception {

        Properties properties = new Properties();
        LOGGER.debug("Extract process started...");
        try {
        	if(args.length == 0){
            	LOGGER.info("Please pass the configuration file in the argument!");
            	return;
            }
            String propertyFilePath = args[0];
            properties = Util.getProperties(propertyFilePath, "PASSWORD", "CLIENT_CREDENTIALS");
        } catch (Exception e) {
            LOGGER.error("Failed to Read the Properties File :: ");
            e.printStackTrace();
            return;
        }
        
        // Validate the Input data provided		
		Map<List<String>, List<String>> mutualExclusiveProps = new HashMap<>();
		mutualExclusiveProps.put(Arrays.asList("PASSWORD","USERNAME"), Arrays.asList("CLIENT_CREDENTIALS"));
		List<String> missingKeys = Util.listMissingProperties(properties,
                Arrays.asList("ENVIRONMENT_URL", "TENANT_ID", "AUTH_URL", "OUTPUT_LOCATION","EXTRACT_TYPE"), mutualExclusiveProps);

        if (!missingKeys.isEmpty()) {
        	LOGGER.error("Following properties are missing from configuration file!! \n" + missingKeys);
            System.exit(0);
        }

        final RDMDataExtractConfigProperties configProperties = new RDMDataExtractConfigProperties(properties);

		Util.setHttpProxy(properties);

		Date startDate = new Date();

        ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors
                .newFixedThreadPool(RDMDataExtractConstants.THREAD_COUNT);

        ReltioAPIService apiService = Util.getReltioService(properties);
        
        Map<String, String> map =  getLookupNameTypeMap(apiService, configProperties);
        
		List<Future<?>> futures = new ArrayList<Future<?>>();
		
		
		if(configProperties.getLookupTypes() == null || configProperties.getLookupTypes().size() == 0) {
	        for (Map.Entry<String, String> entry : map.entrySet()) {

				Extract ec = new Extract(entry.getKey(), entry.getValue(), configProperties, apiService, configProperties.getExtractType());
				Future<?> f = executorService.submit(ec);
				futures.add(f);

	        }
		}else {
	        for (String lookupType : configProperties.getLookupTypes()) {

				Extract ec = new Extract(lookupType, map.get(lookupType), configProperties, apiService, configProperties.getExtractType());
				Future<?> f = executorService.submit(ec);
				futures.add(f);

	        }
		}


		for(Future<?> future : futures) {
			future.get(); // get will block until the future is done
		}
			
        executorService.shutdown();
		LOGGER.debug("RDM data extraction Complete");
		
		Date endDate = new Date();

		long diff = endDate.getTime() - startDate.getTime();

		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);

		LOGGER.debug("Processing Time = "+diffDays + " days, "+diffHours + " hours, "+diffMinutes + " minutes, "+diffSeconds + " seconds.");

    }


    /**
     * Get lookup types map
     * @param apiService
     * @param configProperties
     * @return
     */
    private static Map<String, String> getLookupNameTypeMap(ReltioAPIService apiService, RDMDataExtractConfigProperties configProperties){
    	Map<String, String> map = new HashMap<>();
		List<LookupType> responseList = new ArrayList<>();
    	
    	
		for(int i = 0; i < RDMDataExtractConstants.RETRY_COUNT; i++) {
			String response;
			try {
				response = apiService.get(configProperties.getBaseConfURL(), null);
		        Gson gson = new Gson();
		        LookupResult result = gson.fromJson(response, new TypeToken<LookupResult>() {
		        }.getType());
		        
		        responseList = result.getLookupTypes();
		        
			} catch (GenericException | ReltioAPICallFailureException e) {
				LOGGER.error("Error in getting RDM data. Error = "+e.getMessage(), e);
				continue;
			}
	        
			break;
		}

    	if(responseList.size() > 0) {
    		for(LookupType type : responseList) {
    			String code = type.uri.replace("rdm/lookupTypes/", "");
    			if(configProperties.getLookupTypes() == null || configProperties.getLookupTypes().size() == 0 || configProperties.getLookupTypes().contains(code)) {
    				map.put(code, type.label);
    			}
    		}
    	}
		
    	
    	return map;
    }
    
}
