/**
 * 
 */
package com.reltio.rdm.extract.domain;

/**
 * @author sanjay
 *
 */
public class LookupType {
	public String uri;
	public String label;
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	
}
