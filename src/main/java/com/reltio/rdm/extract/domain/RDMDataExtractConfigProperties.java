package com.reltio.rdm.extract.domain;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.reltio.rdm.extract.task.Extract.ExtractType;

/**
 * Created by Sanjay on 14/09/17.
 */
public class RDMDataExtractConfigProperties implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(RDMDataExtractConfigProperties.class.getName());

    /**
	 * 
	 */
	private static final long serialVersionUID = -7510382433350898901L;
    private String serverHostName;
    private String tenantId;
    private String username;
    private String password;
    private String authURL;
    private String baseDataloadURL;
    private String baseConfURL;
	private List<String> lookupTypes;
	private String outputLocation;
	private ExtractType extractType;
	
	
    public RDMDataExtractConfigProperties(Properties properties) {
    	boolean isMissingProp = false;
    	
    	
    	if(!checkNull(properties.getProperty("OUTPUT_LOCATION"))) {
    		logger.debug("OUTPUT_LOCATION is mandatory");
    		isMissingProp = true;
    	}

    	if(!checkNull(properties.getProperty("ENVIRONMENT_URL"))) {
    		logger.debug("ENVIRONMENT_URL is mandatory");
    		isMissingProp = true;
    	}
    	
    	if(!checkNull(properties.getProperty("TENANT_ID"))) {
    		logger.debug("TENANT_ID is mandatory");
    		isMissingProp = true;
    	}

    	if(!checkNull(properties.getProperty("AUTH_URL"))) {
    		logger.debug("AUTH_URL is mandatory");
    		isMissingProp = true;
    	}
    	
    	if(checkNull(properties.getProperty("EXTRACT_TYPE"))) {
    		try {
    			extractType = ExtractType.valueOf(properties.getProperty("EXTRACT_TYPE"));
    		}catch (Exception e) {
        		extractType = ExtractType.RAW; 
			}
    		
    	}else {
    		extractType = ExtractType.RAW; 
    	}
    	
    	if(isMissingProp) {
    		System.exit(-1);
    	}
    	
    	outputLocation = properties.getProperty("OUTPUT_LOCATION");
        serverHostName = properties.getProperty("ENVIRONMENT_URL");
        tenantId = properties.getProperty("TENANT_ID");
        username = properties.getProperty("USERNAME");
        password = properties.getProperty("PASSWORD");
        authURL = properties.getProperty("AUTH_URL");


        if (checkNull(serverHostName) && checkNull(tenantId)) {
            baseDataloadURL = "https://" + serverHostName + "/lookups/"
                    + tenantId;
            setBaseConfURL("https://" + serverHostName + "/configuration/"
                    + tenantId);
        }
        

        if(checkNull(properties.getProperty("LOOKUP_TYPES"))) {
            lookupTypes = Arrays.asList(properties.getProperty("LOOKUP_TYPES").split(","));
        }

        logger.debug("ENVIRONMENT_URL = "+serverHostName);
        logger.debug("TENANT_ID = "+tenantId);
        logger.debug("USERNAME = "+username);
        logger.debug("PASSWORD = "+password);
        logger.debug("AUTH_URL = "+authURL);
        logger.debug("LOOKUP_TYPES = "+lookupTypes);
    }

    public static boolean checkNull(String value) {
        if (value != null && !value.trim().equals("")
                && !value.trim().equals("UNKNOWN")
                && !value.trim().equals("<blank>")
                && !value.trim().equals("<UNAVAIL>")
                && !value.trim().equals("#")
                && !value.toLowerCase().trim().equals("null")
                && !value.toLowerCase().trim().equals("\"")) {
            return true;
        }
        return false;
    }

    public String getServerHostName() {
        return serverHostName;
    }

    public void setServerHostName(String serverHostName) {
        this.serverHostName = serverHostName;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthURL() {
        return authURL;
    }

    public void setAuthURL(String authURL) {
        this.authURL = authURL;
    }

    public String getBaseDataloadURL() {
        return baseDataloadURL;
    }

    // Function to ignore null values

    public void setBaseDataloadURL(String baseDataloadURL) {
        this.baseDataloadURL = baseDataloadURL;
    }

	public List<String> getLookupTypes() {
		return lookupTypes;
	}

	public void setLookupTypes(List<String> lookupTypes) {
		this.lookupTypes = lookupTypes;
	}

	public String getOutputLocation() {
		return outputLocation;
	}

	public void setOutputLocation(String outputLocation) {
		this.outputLocation = outputLocation;
	}

	public String getBaseConfURL() {
		return baseConfURL;
	}

	public void setBaseConfURL(String baseConfURL) {
		this.baseConfURL = baseConfURL;
	}

	public ExtractType getExtractType() {
		return extractType;
	}

	public void setExtractType(ExtractType extractType) {
		this.extractType = extractType;
	}
}
