/**
 * 
 */
package com.reltio.rdm.extract.domain;

import java.util.List;

/**
 * @author sanjay
 *
 */
public class LookupResult {
	public List<LookupType> lookupTypes;

	public List<LookupType> getLookupTypes() {
		return lookupTypes;
	}

	public void setLookupTypes(List<LookupType> lookupTypes) {
		this.lookupTypes = lookupTypes;
	}
	
}
