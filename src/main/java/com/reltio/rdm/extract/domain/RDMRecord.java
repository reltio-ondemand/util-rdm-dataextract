/**
 * 
 */
package com.reltio.rdm.extract.domain;

import java.util.List;

/**
 * @author sanjay
 *
 */
public class RDMRecord {
	private String code;
	private List<String> parents;
	private List<Attribute> attributes;
	private List<SourceMapping> sourceMappings;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public List<String> getParents() {
		return parents;
	}
	public void setParents(List<String> parents) {
		this.parents = parents;
	}
	public List<Attribute> getAttributes() {
		return attributes;
	}
	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}
	public List<SourceMapping> getSourceMappings() {
		return sourceMappings;
	}
	public void setSourceMappings(List<SourceMapping> sourceMappings) {
		this.sourceMappings = sourceMappings;
	}
	
	
}


