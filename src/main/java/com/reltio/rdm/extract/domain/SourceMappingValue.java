/**
 * 
 */
package com.reltio.rdm.extract.domain;

/**
 * @author sanjay
 *
 */
public class SourceMappingValue{
	public String code;
	public String value;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
