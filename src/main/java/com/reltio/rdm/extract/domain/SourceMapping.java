/**
 * 
 */
package com.reltio.rdm.extract.domain;

import java.util.List;

/**
 * @author sanjay
 *
 */
public class SourceMapping {
	public String source;
	public List<SourceMappingValue> values;
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	
	public List<SourceMappingValue> getValues() {
		return values;
	}

	public void setValues(List<SourceMappingValue> values) {
		this.values = values;
	}
	
}